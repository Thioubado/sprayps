<?php
/* Smarty version 3.1.33, created on 2020-08-09 02:28:59
  from 'C:\laragon\www\spray\themes\classic\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5f2f515b9d4e51_67069188',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ffdb7ffbdd2955cd1ff40119dd9d033cb4f99f7d' => 
    array (
      0 => 'C:\\laragon\\www\\spray\\themes\\classic\\templates\\page.tpl',
      1 => 1593780665,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f2f515b9d4e51_67069188 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1175796755f2f515b9c9391_17756490', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_1632358395f2f515b9ca232_08003074 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_3743345645f2f515b9c9a30_38339730 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1632358395f2f515b9ca232_08003074', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_17760792065f2f515b9d0663_77564734 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_11301427545f2f515b9d1519_40313282 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_21250455125f2f515b9cfb69_12042525 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17760792065f2f515b9d0663_77564734', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11301427545f2f515b9d1519_40313282', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_2423335485f2f515b9d3471_78194403 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_18348560045f2f515b9d2a27_79116488 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2423335485f2f515b9d3471_78194403', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_1175796755f2f515b9c9391_17756490 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_1175796755f2f515b9c9391_17756490',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_3743345645f2f515b9c9a30_38339730',
  ),
  'page_title' => 
  array (
    0 => 'Block_1632358395f2f515b9ca232_08003074',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_21250455125f2f515b9cfb69_12042525',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_17760792065f2f515b9d0663_77564734',
  ),
  'page_content' => 
  array (
    0 => 'Block_11301427545f2f515b9d1519_40313282',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_18348560045f2f515b9d2a27_79116488',
  ),
  'page_footer' => 
  array (
    0 => 'Block_2423335485f2f515b9d3471_78194403',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3743345645f2f515b9c9a30_38339730', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21250455125f2f515b9cfb69_12042525', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18348560045f2f515b9d2a27_79116488', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
