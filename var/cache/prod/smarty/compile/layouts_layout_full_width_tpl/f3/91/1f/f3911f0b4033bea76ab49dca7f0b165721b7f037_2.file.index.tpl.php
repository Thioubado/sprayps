<?php
/* Smarty version 3.1.33, created on 2020-08-09 02:28:59
  from 'C:\laragon\www\spray\themes\classic\templates\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5f2f515b9acb72_40106065',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3911f0b4033bea76ab49dca7f0b165721b7f037' => 
    array (
      0 => 'C:\\laragon\\www\\spray\\themes\\classic\\templates\\index.tpl',
      1 => 1593780665,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5f2f515b9acb72_40106065 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7906653235f2f515b9a0da6_28602289', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_7725159625f2f515b9a54d9_15506429 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_5478393205f2f515b9a9ce3_13482474 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_6880136335f2f515b9a8f23_52461726 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5478393205f2f515b9a9ce3_13482474', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_7906653235f2f515b9a0da6_28602289 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_7906653235f2f515b9a0da6_28602289',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_7725159625f2f515b9a54d9_15506429',
  ),
  'page_content' => 
  array (
    0 => 'Block_6880136335f2f515b9a8f23_52461726',
  ),
  'hook_home' => 
  array (
    0 => 'Block_5478393205f2f515b9a9ce3_13482474',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7725159625f2f515b9a54d9_15506429', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6880136335f2f515b9a8f23_52461726', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
